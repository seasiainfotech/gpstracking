//
//  JourneyController.swift
//  GPSTrackerSwift
//
//  Created by Pooja Rana on 07/06/16.
//  Copyright © 2016 Pooja Rana. All rights reserved.
//

import UIKit
import MapKit

class JourneyController: UIViewController, CLLocationManagerDelegate,MKMapViewDelegate {

    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var pace: UILabel!
    @IBOutlet weak var startStopButton: UIButton!
    
    var seconds: Int32 = 0
    var distanceValue: Double = 0.0
    var locations = NSMutableArray()
    var timer : NSTimer!
    
    // MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBOutlet Action
    @IBAction func startStopJourneyAction(sender: UIButton) {
        if (sender.titleForState(UIControlState.Normal)! as String) == "Start"
        {
            sender.setTitle("Stop", forState: UIControlState.Normal)
            seconds = 0;
            distanceValue = 0.0;
            timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(JourneyController.eachSecond), userInfo: nil, repeats: true)
           self.startLocationUpdates()
        }
        else
        {
            sender.setTitle("Start", forState: UIControlState.Normal)
            appDelegate.locationManager.stopUpdatingLocation()
            timer.invalidate()
            
            let alertController=UIAlertController(title: "GPSTracker", message: "Do you want to save this run?", preferredStyle: UIAlertControllerStyle.Alert);
            
            alertController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
                self.saveRun()
                self.navigationController?.popViewControllerAnimated(true)
            }))
            
            alertController.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
                if(self.appDelegate.locationManager != nil){
                    self.appDelegate.locationManager.delegate=nil;
                }
                self.navigationController?.popViewControllerAnimated(true)
            }))
            presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func eachSecond() {
        seconds += 1;
        
        time.text = CalculationController.stringifySecondCount(seconds, usingLongFormat: false)
        distance.text = CalculationController.stringifyDistance(distanceValue)
        pace.text = CalculationController.stringifyAvgPaceFromDist(distanceValue, overTime: seconds)
    }

    // MARK: - LocationManager Delegates
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        for newLocation in locations{
            
            let eventDate = newLocation.timestamp
            let howRecent = eventDate.timeIntervalSinceNow
            
            if (fabs(howRecent) < 10.0 && newLocation.horizontalAccuracy < 20) {
                // update distance
                
                if (self.locations.count > 0) {
                    distanceValue += newLocation.distanceFromLocation(self.locations.lastObject as! CLLocation)
                    
                    let coords1 = (self.locations.lastObject as! CLLocation).coordinate
                    let coords2 = newLocation.coordinate
                    
                    let region = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 500, 500)
                    mapView.setRegion(region, animated: true)
                    
                    var points: [CLLocationCoordinate2D]
                    points = [coords1, coords2]
                    mapView.addOverlay(MKPolyline(coordinates: &points, count: 2))
                }
                self.locations.addObject(newLocation)
            }
        }
    }
    
    func startLocationUpdates()
    {
    // Create the location manager if this object does not
    // already have one.
       let  locationAllowed = CLLocationManager.locationServicesEnabled()
        
        let status = CLLocationManager.authorizationStatus()
        if (locationAllowed == false || status == CLAuthorizationStatus.Denied){
            
            let alertController=UIAlertController(title: "GPSTracker \n Location Service Disabled", message: "Your GPS is disabled. GPSTracker requires this to be activated, please turn your GPS on to continue. Do you want to enable it now?", preferredStyle: UIAlertControllerStyle.Alert);
            
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
            }))
            presentViewController(alertController, animated: true, completion: nil)
        }
        else
        {
             if appDelegate.locationManager != nil {
                if (status==CLAuthorizationStatus.NotDetermined){
                    
                    if appDelegate.locationManager.respondsToSelector(#selector(CLLocationManager.requestAlwaysAuthorization)){
                        appDelegate.locationManager.requestAlwaysAuthorization()
                    }
                }
            
                appDelegate.locationManager.delegate=self
                appDelegate.locationManager.pausesLocationUpdatesAutomatically = false
                appDelegate.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                appDelegate.locationManager.distanceFilter = 0
            }
          appDelegate.locationManager.startUpdatingLocation()
       }
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer
    {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blueColor()
            polylineRenderer.lineWidth = 3
            return polylineRenderer
        }
        
        return MKPolylineRenderer()
    }
    
    func saveRun()
    {
        let mDict:[String : NSObject] = ["Distance":NSNumber(double: distanceValue), "Seconds":NSNumber(int : seconds), "Timestamp":NSDate(), "LocationArray":locations]
        appDelegate.saveRun(mDict)
    }
}
